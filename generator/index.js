module.exports = api => {
  api.render('./template');
  api.extendPackage({
    scripts: {
      serve: "vue-cli-service serve",
      build: "vue-cli-service build",
      lint: "vue-cli-service lint",
      greet: 'vue-cli-service greet'
    },
    dependencies: {
      "core-js": "^2.6.5",
      "vue": "^2.6.10",
      "lodash-es": "^4.17.15",
      "vue-router": "^3.0.3",
      "vuex": "^3.0.1"
    },
    devDependencies: {
      "@vue/cli-plugin-babel": "^3.11.0",
      "@vue/cli-plugin-eslint": "^3.11.0",
      "@vue/cli-service": "^3.11.0",
      "@vue/eslint-config-standard": "^4.0.0",
      "babel-eslint": "^10.0.1",
      "dotenv": "^8.1.0",
      "eslint": "^5.16.0",
      "eslint-config-google": "^0.14.0",
      "eslint-plugin-vue": "^5.0.0",
      "node-sass": "^4.12.0",
      "pug": "^2.0.4",
      "pug-plain-loader": "^1.0.0",
      "sass-loader": "^8.0.0",
      "vue-template-compiler": "^2.6.10"
    },
    eslintConfig: {
      "root": true,
      "env": {
        "node": true,
        "browser": true
      },
      "extends": [
        "plugin:vue/recommended",
        "google"
      ],
      "rules": {
        "generator-star-spacing": "off",
        "no-debugger": "error",
        "brace-style": 2,
        "camelcase": 1,
        "comma-dangle": 0,
        "keyword-spacing": 2,
        "max-len": 0,
        "no-console": 0,
        "no-invalid-this": 0,
        "no-loop-func": 1,
        "no-var": 0,
        "object-curly-spacing": 0,
        "one-var": 0,
        "prefer-rest-params": 1,
        "require-jsdoc": 0,
        "space-infix-ops": 2,
        "valid-jsdoc": 1
      },
      "parserOptions": {
        "parser": "babel-eslint"
      }
    },
  })
}