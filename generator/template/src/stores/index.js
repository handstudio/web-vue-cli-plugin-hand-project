/**
 * 스토어는 각 모듈별로 나눠서 관리한다
 */


import Vue from 'vue';
import Vuex from 'vuex';
import example from './modules/example-store';


Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    example,
  }
});
