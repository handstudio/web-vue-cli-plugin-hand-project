/**
 * 하나의 파일에 state, getters, actions, mutations를 정의한다
 */
// import api from '@/services/api';


const state = {

};

const getters = {

};

const actions = {

};

const mutations = {

};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};